Task-Per-Derivative: 1
Task-Section: user
Task-Description: Ubuntu Unity desktop
Task-Extended-Description: This task provides the Ubuntu Unity desktop environment.
Task-Key: ubuntu-unity-desktop
Task-Seeds: desktop-common

= Hardware and Architecture Support =

== Architecture-independent ==

Bluetooth:

 * (gnome-bluetooth) [!s390x] # desktop bluetooth support

= Network Services =

Basic network services, VPN and Windows integration.

 * (avahi-autoipd)     # IPv4 link-local interface configuration support
 * (network-manager-gnome) # see NetworkRoaming spec
 * (network-manager-pptp-gnome)
 * (libproxy1-plugin-gsettings)
 * (libproxy1-plugin-networkmanager)
 * (ppp)      # ppp utilities useful to be on a CD when someone needs them
 * (pppconfig)
 * (pppoeconf)

= GUI infrastructure =

 * policykit-1-gnome            # workaround to avoid gnome-shell
 * xterm                        # Provide a backup terminal and complete X env.
 * libnotify-bin                # various scripts need notify-send (such as .bashrc's "alert")

Input methods:

 * (im-config)
 * (ibus)
 * (ibus-gtk)
 * (ibus-gtk3)
 * (ibus-table)

Desktop Experience:
 * unity [!s390x]
 * unity-greeter [!s390x]
 * notify-osd
 * (activity-log-manager)
 * zeitgeist-core
 * zeitgeist-datahub
 * unity-settings-daemon
 * unity-control-center [!s390x]
 * unity-scopes-runner
 * unity-lens-applications
 * unity-lens-files

= Desktop apps =

We use many GNOME applications as part of our desktop user interface. Rather than using Debian's meta-packages they are deliberately expanded so that we can select things a bit better.

 * (apport-gtk)
 * (whoopsie)
 * eog
 * (geary)
 * file-roller
 * (gnome-calculator)
 * lightdm [!s390x]
 * gedit
 * (app-install-data-partner)
 * (transmission-gtk)
 * gnome-menus
 * (system-config-printer)
 * unity-session
 * (gnome-system-monitor)
 * (gnome-power-manager)
 * gnome-terminal
 * (gnome-todo)
 * (baobab)
 * (gnome-screenshot)
 * (gnome-system-log)
 * (gnome-logs)         # ubuntu ships gnome-logs
 * (gnome-font-viewer)
 * (gucharmap)
# XXX: ask QA if we need this in the desktop install or just live
 * language-selector-gnome
 * (firefox)
 * (xul-ext-ubufox)
 * (nautilus)
 * (nautilus-sendto)
 * (nautilus-share)
 * (rhythmbox)
 * (rhythmbox-zeitgeist)
 * (cheese)
 * software-properties-gtk
 * (totem)
 * ubuntu-release-upgrader-gtk
 * update-manager
 * update-notifier
 * (vino) # vnc server
 * yelp
 * zenity
 * (xdg-utils)         # useful utilities
 * xdg-user-dirs
 * xdg-user-dirs-gtk
 * pulseaudio
 * (pulseaudio-module-bluetooth)
 * (libglib2.0-bin)     # mostly used as xdg-open backend (gio open)
 * (gvfs-fuse)          # let non-GNOME apps see GVFS via fuse
 * (gnome-disk-utility)
 * (simple-scan)
 * (evince)
 * (gnome-screensaver)
 * (remmina)
 * (shotwell)
 * (usb-creator-gtk) [i386 amd64]
 * (deja-dup)
 * (gnome-calendar)

Software center
 * (ubuntu-software)
 * (gnome-software-plugin-snap)
 * (synaptic)
 * (gdebi)

= Key management and signing =

 * (gnome-keyring)
 * (seahorse)
 * (libpam-gnome-keyring)

We use gnupg's pinentry support as of wily, instead of gnome-keyring as before.

 * (gpg-agent)
 * (dirmngr)

Games: We only ship a few by default.
 * (aisleriot)
 * (gnome-mahjongg)
 * (gnome-mines)
 * (gnome-sudoku)
 * (branding-ubuntu)       # Ubuntu branding for some games

Default office suite: libreoffice

 * (libreoffice-style-breeze)
 * (libreoffice-gnome)
 * (libreoffice-writer)
 * (libreoffice-calc)
 * (libreoffice-impress)
 * (libreoffice-math)
 * (libreoffice-ogltrans)
 * (libreoffice-pdfimport)

Themes: we will ship only the very best looking themes. We won't ship everything that Debian ships, just a custom package that includes the themes we want. These packages are included for size guidance until we have Ubuntu-specific packages.

 * ubuntu-artwork
 * ubuntu-sounds
 * gnome-session-canberra   # Needed to make login/logout sounds audible.
 * dmz-cursor-theme
 * (overlay-scrollbar-gtk2)
 * (fonts-noto-color-emoji)

Boot theme

 * (plymouth-theme-ubuntu-logo)

Settings: we have some settings overrides by default in an unique package.
          and provide our ones, like sound settings
 * ubuntu-settings                  # should be unity-settings in future
 * (gsettings-ubuntu-schemas)       # gsettings-unity-schemas?

The gstreamer1.0 packages we want to install:

 * gstreamer1.0-alsa
 * gstreamer1.0-plugins-base-apps
 * gstreamer1.0-packagekit
 * gstreamer1.0-pulseaudio
 * gstreamer1.0-libav	#For video thumbnails
 * gstreamer1.0-plugins-good
 * gstreamer1.0-plugins-bad
 * gstreamer1.0-plugins-ugly

Accessibility tools:

 * (gnome-accessibility-themes)
 * (orca)
 * (onboard)
 * (brltty)
 * (xcursor-themes)
 * (speech-dispatcher)
 * (mousetweaks)
 * at-spi2-core
 * libatk-adaptor
 * (libgail-common)
 * (a11y-profile-manager-indicator)
 * (unity-accessibility-profiles)


= Documentation =

 * doc-base         # integrates with scrollkeeper
 * (ubuntu-docs)

= Other =

 * ubuntu-unity-desktop # metapackage for everything here
 * (example-content)
 * (libwmf0.2-7-gtk) # provides a gdk-pixbuf loader for WMF images
 * (ppa-purge)
 * (gnome-system-tools)
